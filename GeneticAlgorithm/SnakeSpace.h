#pragma once
#include "ImageWriter.h"
#include <vector>
#include <random>

#define SIZE_RATIO 4
#define SNAKE_POINTS 5

struct Point2d
{
	int x, y;
};

struct Circle
{
	int x, y, r;
};

struct Snake
{
	Point2d points[SNAKE_POINTS];
	Snake(std::mt19937& rng, std::uniform_int_distribution<int>& uni)
	{
		for (Point2d& point : points)
			point = { uni(rng), uni(rng) };
	}
	void DrawSnake(ImageWriter& writer)
	{
		for (int i = 0; i < SNAKE_POINTS - 1; ++i)
			writer.DrawLine(points[i].x, points[i].y, points[i + 1].x, points[i + 1].y);
	}
};

class SnakeSpace
{
	int size;
	int populationSize;
	int numCircles;
	std::vector<Circle> circles;
	std::vector<Snake> currentPopulation;
public:
	SnakeSpace(int size, int numCircles, int populationSize)
		: size(size), numCircles(numCircles), populationSize(populationSize)
	{
		std::random_device rd;
		std::mt19937 rng(rd());
		std::uniform_int_distribution<int> uni(0, size);
		for (int i = 0; i < numCircles; ++i)
			circles.push_back({ uni(rng), uni(rng), uni(rng) / SIZE_RATIO });
		for (int i = 0; i < populationSize; ++i)
			currentPopulation.push_back(Snake(rng, uni));
	}
	void SaveGeneration(int i)
	{
		for (int j = 0; j<populationSize; ++j)
		{
			ImageWriter writer(size, size);
			for (auto& it : circles)
				writer.DrawCircle(it.x, it.y, it.r);
			currentPopulation[j].DrawSnake(writer);
			writer.SaveImage(std::wstring(L"gen") + std::to_wstring(i) + std::wstring(L"-") + std::to_wstring(j) + std::wstring(L".png"));
		}
	}
};
